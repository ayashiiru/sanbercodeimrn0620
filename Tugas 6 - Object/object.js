// Soal 1 //
function arrayToObject(array){
    for(var i=0; i < array.length; i++){
        var now = new Date()
        var thisYear = now.getFullYear()
        var birthYear = array[i][3]
        var calcAge = ""
        if(birthYear === undefined || birthYear > thisYear){
             calcAge = "Invalid birth year"
        }
        else{
            calcAge = thisYear - birthYear
        }
        var personObj = {
            firstName: array[i][0],
            lastName: array[i][1],
            gender: array[i][2],
            age: calcAge
        }
        console.log(personObj)
    }
    
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// Soal 2 //

function shoppingTime(memberId, money) {
    // you can only write your code here!
    if (memberId === undefined || memberId == ''){
        return "Mohon maaf, toko X hanya berlaku untuk member saja."
    }
        else{
            id = memberId
        
            if (money < 50000){
                return "Mohon maaf, uang tidak cukup"
            }
            else{
                ownedMoney = money

            var saleList = {
                stacattu: 1500000,
                zoro: 500000,
                h_n: 250000,
                uniklooh: 175000,
                casing: 50000 
            }

            var shoppedList = []
            var initialMoney = ownedMoney
            var price = 0

            if(ownedMoney >= 1500000){
                shoppedList.push("Sepatu Stacattu")
                ownedMoney = ownedMoney - saleList.stacattu
                price = price + saleList.stacattu
            }
            
            if(ownedMoney >=500000){
                shoppedList.push("Baju Zoro")
                ownedMoney = ownedMoney - saleList.zoro
                price = price + saleList.zoro
            }

            if(ownedMoney >= 250000){
                shoppedList.push("Baju H&N")
                ownedMoney = ownedMoney - saleList.h_n
                price = price + saleList.h_n
            }

            if(ownedMoney >= 175000){
                shoppedList.push("Sweater Uniklooh")
                ownedMoney = ownedMoney - saleList.uniklooh
                price = price + saleList.uniklooh
            }

            if(ownedMoney >= 50000){
                shoppedList.push("Casing Handphone")
                ownedMoney = ownedMoney - saleList.casing
                price = price + saleList.casing
            }

            var memberObj = {
                member: memberId,
                wallet: initialMoney,
                listPurchased: shoppedList,
                changeMoney: initialMoney - price
                }
            
            
            } 
        }
        return memberObj
}

  // TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3 //

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    var output = []
    for(var i=0; i < arrPenumpang.length; i++){

        var passenger = arrPenumpang[i][0]
        var origin = arrPenumpang[i][1]
        var destination = arrPenumpang[i][2]

    var originNumber = 0
    var destinationNumber = 0

    switch(origin){
        case 'A': originNumber = 1; break;
        case 'B': originNumber = 2; break;
        case 'C': originNumber = 3; break;
        case 'D': originNumber = 4; break;
        case 'E': originNumber = 5; break;
        case 'F': originNumber = 6; break;
        default: originNumber = 1; break;
    }
    
    switch(destination){
        case 'A': destinationNumber = 1; break;
        case 'B': destinationNumber = 2; break;
        case 'C': destinationNumber = 3; break;
        case 'D': destinationNumber = 4; break;
        case 'E': destinationNumber = 5; break;
        case 'F': destinationNumber = 6; break;
        default: destinationNumber = 1; break;
    }

    var trip = 0
    while(originNumber<destinationNumber){
        trip = trip + 1
        originNumber++
    }

    var cost = trip * 2000

    var penumpangObj = {
        penumpang: passenger,
        naikDari: origin,
        tujuan: destination,
        bayar: cost
    }

    output.push(penumpangObj)
  }
  return output
}   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]