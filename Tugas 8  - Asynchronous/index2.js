var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

var i = 0
function read(times){
    if(i < books.length){
        readBooksPromise(times, books[i])
        .then((fulfilled) => read(fulfilled))
        .catch((rejected) => console.log(rejected))
    }
    i++
}

read(10000)