// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini

let i = 0
function read(times){
    if (i < books.length) {
        readBooks(times, books[i], (sisaWaktu) => read(sisaWaktu))
    }
    i++
}

read(10000)
