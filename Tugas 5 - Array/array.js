// Soal 1 //

function range(startNum, finishNum){
    var numbers = [startNum]
    if (startNum < finishNum) {
        while (startNum < finishNum) {
            numbers.push(startNum + 1)
            startNum++
        } 
    }
    else if (startNum > finishNum) {
        while (startNum > finishNum) {
            numbers.push(startNum - 1)
            startNum--
        }
    }
    else if (startNum === undefined || finishNum === undefined){
        numbers.shift()
        numbers.push(-1)
    }
    
    return numbers
}

console.log(range(1))

// Soal 2 //

function rangeWithStep(startNum, finishNum, step){
    var numbers = [startNum]
    if (startNum < finishNum) {
        while (startNum + step < finishNum) {
            numbers.push(startNum + step)
            startNum = startNum + step
        } 
    }
    else if (startNum - step > finishNum) {
        while (startNum > finishNum) {
            numbers.push(startNum - step)
            startNum = startNum - step
        }
    }
    else if (startNum === undefined || finishNum === undefined){
        numbers.shift()
        numbers.push(-1)
    }
    
    return numbers
}

console.log(rangeWithStep(1, 10, 2))

// Soal 3 //

function sum(startNum, finishNum, step){
    var numbers = [startNum]
    if (startNum < finishNum) {
        while (startNum < finishNum) {
            numbers.push(startNum + 1)
            startNum++
        } 
    }
    else if (startNum > finishNum) {
        while (startNum > finishNum) {
            numbers.push(startNum - 1)
            startNum--
        }
    }
    else if (startNum === undefined || finishNum === undefined){
        numbers.shift()
        numbers.push(-1)
    }

    var arrlength = numbers.length

    for (var i = 0; i < arrlength; i++){
       var result = result + numbers[i]
    }
    return result
    
}

console.log(sum(1, 10))

// Soal 4 //

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(){
    for(var i=0; i < input.length; i++) {
        
        var id = input[i][0]
        var name = input[i][1]
        var dob = input[i][2] + " " + input[i][3]
        var hobby = input[i][4]

        console.log("ID: " + id)
        console.log("Nama: " + name)
        console.log("TTL: " + dob)
        console.log("Hobby: " + hobby)
    }
}

dataHandling(input)

// Soal 5 //

function balikKata(string){
    var output = ""
    for(var i = string.length - 1; i >= 0; i--){
        output = output + string[i]
    }
    return output
}
console.log(balikKata("Kasur Rusak"))

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 

function dataHandling2(){
    input.splice(2, 1, "Provinsi " + input[2])
    input.splice(4, 1, "Pria", "SMA Internasional Metro")
    var date = input[3].split("/")
    var month = date[1]

    switch(month) {
        case '01': {month = "Januari"; break;}
        case '02': {month = "Feburari"; break;}
        case '03': {month = "Maret"; break;}
        case '04': {month = "April"; break;}
        case '05': {month = "Mei"; break;}
        case '06': {month = "Juni"; break;}
        case '07': {month = "Juli"; break;}
        case '08': {month = "Agustus"; break;}
        case '09': {month = "September"; break;}
        case '10': {month = "Oktober"; break;}
        case '11': {month = "November"; break;}
        case '12': {month = "Desember"; break;}
        default: { console.log('default');}
    }

    var sortedDate = date.sort(function(a,b){return a-b})
    var reversedDate = sortedDate.reverse()
    var concat = date.join("-")

    var name = input[1]

    if(name.length > 15) {
        name = name.pop()
    }

    console.log(input)
    console.log(month)
    console.log(reversedDate)
    console.log(concat)
    console.log(name)

}

dataHandling2(input)
