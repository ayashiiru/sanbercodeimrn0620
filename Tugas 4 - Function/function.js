// Soal 1 //

function teriak(){
    return "Halo Sanbers!"
}

console.log(teriak())

// Soal 2 //

function kalikan(first_num, second_num){
    var result = first_num * second_num
    return result
}

var num1 = 12
var num2 = 4
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

// Soal 3 //

function introduce(name, age, address, hobby){
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + " dan saya punya hobby yaitu " + hobby + "!"
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
