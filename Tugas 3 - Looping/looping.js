// Soal 1 //
var i = 2
console.log('LOOPING PERTAMA')
while(i <= 20 ) {
    console.log(i + ' - I love coding')
    i = i+2
}

var b = 20
console.log('LOOPING KEDUA')
while (b >= 2){
    console.log(b + ' - I will become a mobile developer')
    b = b-2
}

// Soal 2 //

console.log('OUTPUT')
for (var c = 1; c <= 20; c++){
    if ( c%3 == 0 && c%2 != 0){
        console.log( c + ' - I Love Coding')
    }
    else if( c%2 != 0) {
        console.log( c + ' - Santai')
    }
    else if ( c%2 == 0){
        console.log( c + ' - Berkualitas')
    }
    
}

// Soal 3 //

for (var d = 1; d <= 4; d++) {
    console.log('########')
}

// Soal 4 //

for(var stair = '#'; stair.length <= 7; stair += '#'){
    console.log(stair)
}

// Soal 5 //

for (var check = 1; check <= 8; check++){
    if(check%2 == 0) {
        console.log('# # # #')
    }
    else{
        console.log(' # # # #')
    }
}