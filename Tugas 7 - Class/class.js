// Soal 1 //

class Animal{
    constructor(name){
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal{
    constructor(name){
        super(name)
        this.legs = 2 
        this.cold_blooded = false
        this.activity = "Auooo"
    }
    
    yell(){
        console.log(this.activity)
    }

}

class Frog extends Animal{
    constructor(name){
        super(name)
        this.legs = 4
        this.cold_blooded = true
        this.activity = "hop hop"
    }

    jump(){
        console.log(this.activity)
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"

// Soal 2 //

class Clock{
    constructor({template}){
        this.template1 = template
        this.timer = 0
        
    }

    render(){
        this.date = new Date()
        this.hours = this.date.getHours()
        if(this.hours < 10) {this.hours = '0' + this.hours}

        this.mins = this.date.getMinutes();
        if (this.mins < 10) {this.mins = '0' + this.mins}
  
        this.secs = this.date.getSeconds();
        if (this.secs < 10) {this.secs = '0' + this.secs} 

        var output = this.template1
        .replace("h", this.hours)
        .replace("m", this.mins)
        .replace("s", this.secs)
        console.log(output)

        }

        

    stop(){
        clearInterval(this.timer)
    }

    start(){
        this.render()
        this.timer = setInterval(this.render.bind(this), 1000)
    }
}


var clock = new Clock({template: 'h:m:s'});
  clock.start(); 

